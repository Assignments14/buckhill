<?php

namespace Buckhill\Progressor;

use Finite\Exception\ObjectException;
use Finite\State\State;
use Finite\State\StateInterface;
use Finite\StatefulInterface;
use Finite\StateMachine\StateMachine;

class OrderWorkflow implements StatefulInterface
{
    private $state;

    private $stateMachine;

    /**
     * @throws ObjectException|\Finite\Exception\ObjectException
     */
    public function __construct(
    )
    {
        $this->stateMachine = new StateMachine();

        $this->stateMachine->addState(new State('state_0', StateInterface::TYPE_INITIAL));
        $this->stateMachine->addState('state_1');
        $this->stateMachine->addState(new State('state_2', StateInterface::TYPE_FINAL));

        $this->stateMachine->addTransition('checkout', 'state_0', 'state_1');
        $this->stateMachine->addTransition('validate', 'state_1', 'state_2');

        $this->stateMachine->setObject($this);
        $this->stateMachine->initialize();
    }

    public function getFiniteState()
    {
        return $this->state;
    }

    public function setFiniteState($state)
    {
        $this->state = $state;
    }
}