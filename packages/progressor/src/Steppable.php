<?php

namespace Buckhill\Progressor;

use Finite\State\State;
use Workflow\Workflow;
use Workflow\WorkflowStub;

trait Steppable
{
    private OrderWorkflow $workflow;

    public function setGraph(OrderWorkflow $workflow): WorkflowStub
    {
        $this->workflow = $workflow;
        return $this->workflow;
    }

    public function getCurrentState(): bool|string
    {
        return $this->workflow->getStateMachine()->getCurrentState();
    }
}