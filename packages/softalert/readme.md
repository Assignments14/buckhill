#### Buckhill "Order Status Changed" Alerts (buckhill/softalert Laravel package)


#### Instalation:
- add ```Buckhill\SoftAlert\Providers\SoftAlertProvider``` to app service providers (config/app.php)
- execute ```php artisan vendor:publish``` command and choose newly added provider.
- change ```ms_team_webhook_url``` in config/order_alerts.php
- change order status and start receiving notifications