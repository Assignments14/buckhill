<?php

namespace Buckhill\SoftAlert\Providers;

use App\Models\Order;
use Buckhill\SoftAlert\Events\OrderStatusChanged;
use Buckhill\SoftAlert\Listeners\SendTeamNotification;
use Buckhill\SoftAlert\Observers\OrderObserver;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider;

class SoftAlertProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->publishes([
            __DIR__ . '/../config/order_alerts.php' => config_path('order_alerts.php'),
        ]);

        Event::listen(
            OrderStatusChanged::class, [SendTeamNotification::class]
        );

        Order::observe(OrderObserver::class);
    }
}