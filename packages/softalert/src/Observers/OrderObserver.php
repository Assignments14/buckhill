<?php

namespace Buckhill\SoftAlert\Observers;

use App\Models\Order;
use Buckhill\SoftAlert\Events\OrderStatusChanged;
use Illuminate\Support\Facades\Log;

class OrderObserver
{
    private static string $lastOrderStatus;

    public function created(Order $order): void
    {
        self::$lastOrderStatus = $order->order_status_id;
    }

    public function updating(Order $order): void
    {
        self::$lastOrderStatus = $order->order_status_id;
        Log::debug('updating: ', $order->toArray());
    }

    public function updated(Order $order): void
    {
        Log::debug('updated: ', $order->toArray());
        if (self::$lastOrderStatus !== $order->order_status_id) {
            OrderStatusChanged::dispatch($order);
        }
    }
}