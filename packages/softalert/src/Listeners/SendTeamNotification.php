<?php

namespace Buckhill\SoftAlert\Listeners;

use Buckhill\SoftAlert\Events\OrderStatusChanged;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class SendTeamNotification
{
    public function __invoke(OrderStatusChanged $event): void
    {
        $order = $event->order;

        $text = 'Order Status Changed';
        $uuid = $order->uuid;
        $status = $order->status->title;
        $updated_at = $order->updated_at;

        try {
            $webhook_url = config('order_alerts.ms_team_webhook_url');
            $message_data = compact('text', 'uuid', 'status', 'updated_at');
            Http::withHeaders(['Content-Type' => 'application/json'])
                ->post($webhook_url, $message_data);
            Log::debug('Message sent to MS Teams: ', $message_data);
        } catch (\Throwable $e) {
            Log::error('FAILED to send Message to MS Teams: ', (array)$e);
        }
    }
}