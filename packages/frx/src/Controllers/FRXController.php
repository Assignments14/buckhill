<?php

namespace Buckhill\FRX\Controllers;

use App\Support\CustomApiResponse;
use Buckhill\FRX\FRX;
use Buckhill\FRX\Requests\FrxRequest;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class FRXController
{
    public const DEFAULT_CCY = 'EUR';

    use CustomApiResponse;

    /**
     * @throws \JsonException
     */
    public function __invoke(FrxRequest $request): JsonResponse
    {
        $validated = $request->validated();

        $validated['ccy'] = $validated['ccy'] ?? self::DEFAULT_CCY;

        $exchange = (new FRX)->convert($validated['amount'], $validated['ccy']);

        if ($exchange === 0) {
            return $this->responseError('ECB rate is not available', Response::HTTP_EXPECTATION_FAILED);
        }

        return $this->responseSuccessWithData(compact('exchange'));
    }
}