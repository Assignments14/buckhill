<?php

namespace Buckhill\FRX;

use Illuminate\Support\Facades\Http;

class FRX
{
    /**
     * @throws \JsonException
     */
    protected function retrieveRateFor(string $ccy): float
    {
        if (strtolower($ccy) === 'eur') {
            return 1;
        }

        $ccyRate = 0;

        $url = config('frx.ecb_rate_endpoint', 'fuck');

        try {
            $response = Http::get($url);
        } catch (\Throwable) {
            // todo:  cache rates in DB for network/endpoint down issues
            return $ccyRate;
        }

        $xml = simplexml_load_string($response->body());
        $vals = json_decode(json_encode($xml, JSON_THROW_ON_ERROR), true, 512, JSON_THROW_ON_ERROR);

        $rates = $vals['Cube']['Cube']['Cube'];

        foreach ($rates as $rate) {
            if ($rate['@attributes']['currency'] === $ccy) {
                return $rate['@attributes']['rate'];
            }
        }

        return $ccyRate;
    }

    /**
     * @throws \JsonException
     */
    public function convert(int $amount, string $ccy = 'EUR'): int
    {
        $frx = $this->retrieveRateFor($ccy);
        return (int) ($amount * $frx);
    }
}