<?php

use Buckhill\FRX\Controllers\FRXController;
use Illuminate\Support\Facades\Route;

Route::get('frx', FRXController::class)->prefix(env('API_PREFIX', 'api/v1'));
