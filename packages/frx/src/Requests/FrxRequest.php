<?php

namespace Buckhill\FRX\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FrxRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'amount' => 'integer',
            'ccy' => 'string|min:3|max:3|nullable'
        ];
    }
}