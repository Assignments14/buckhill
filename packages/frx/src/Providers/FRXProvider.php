<?php

namespace Buckhill\FRX\Providers;

use Illuminate\Support\ServiceProvider;

class FRXProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->loadRoutesFrom(__DIR__ . '/../routes/frx.php');

        $this->publishes([
            __DIR__ . '/../config/frx.php' => config_path('frx.php'),
        ]);
    }
}