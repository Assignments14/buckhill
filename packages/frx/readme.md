#### FRX Laravel Package

This package exposes an API GET endpoint that receives the following params:

- amount (int)
- currency to exchange (char(3))

and returns given amount converted according to current FRX rate of [European Central Bank](https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml)

The default currency is __Euro__.

The package have unit tests, [swagger documentation](./swagger.json) and a this readme.md file.

The package must be installed with composer as a local dependency.