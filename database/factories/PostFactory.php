<?php

namespace Database\Factories;

use App\Models\File;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Post>
 */
class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'title' => fake()->text(64),
            'slug' => fake()->slug,
            'content' => fake()->realText,
            'metadata' => [
                'author' => fake()->name,
                'image' => File::all()->random()->uuid,
            ],
        ];
    }
}
