<?php

namespace Database\Factories;

use App\Models\File;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Promotion>
 */
class PromotionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     * @throws \Exception
     */
    public function definition(): array
    {
        return [
            'title' => fake()->text,
            'content' => fake()->realText,
            'metadata' => [
                'valid_from' => Carbon::now(),
                'valid_to' => Carbon::now()->days(random_int(3, 23)),
                'image' => File::all()->random()->uuid,
            ]
        ];
    }
}
