<?php

namespace Database\Factories;

use App\Models\OrderStatus;
use App\Models\Payment;
use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Order>
 */
class OrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     * @throws \Exception
     */
    public function definition(): array
    {
        return [
            'uuid' => fake()->uuid(),

            'user_id' => User::all()->random()->id,
            'order_status_id' => OrderStatus::all()->random()->id,
            'payment_id' => Payment::all()->random()->id,

            'products' =>
                $this->repeat(fn() => [
                    'product' => Product::all()->random()->uuid,
                    'quantity' => fake()->randomNumber(2),
                ])
            ,
            'address' => [
                'billing' => fake()->address,
                'shipping' => fake()->address,
            ],

            'delivery_fee' => fake()->randomElement([null, fake()->randomNumber(3)]),
            'amount' => fake()->randomNumber(5),

            'shipped_at' => fake()->randomElement([null, Carbon::now()->days(fake()->randomNumber(1))])
        ];
    }

    /**
     * @throws \Exception
     */
    private function repeat(callable $stencil, ?int $n = null): array
    {
       $n = max(random_int(1, 11), $n);
       $result = [];

       for ($i = 0; $i < $n; $i++) {
           $result[] = $stencil();
       }

       return $result;
    }
}
