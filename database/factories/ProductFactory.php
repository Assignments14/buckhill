<?php

namespace Database\Factories;

use App\Models\Brand;
use App\Models\Category;
use App\Models\File;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'uuid' => fake()->uuid(),
            'category_uuid' => Category::all()->random()->uuid,
            'title' => fake()->linuxProcessor,
            'description' => fake()->realText,
            'price' => fake()->randomNumber(7),
            'metadata' => [
                'brand' => Brand::all()->random()->uuid,
                'image' => File::all()->random()->uuid,
            ]
        ];
    }
}
