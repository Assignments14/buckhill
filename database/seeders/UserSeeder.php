<?php

namespace Database\Seeders;

use App\Enums\MarketingType;
use App\Enums\UserType;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    use WithoutModelEvents;

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Super-Admin predefined
         User::factory()->create([
             'email' => 'admin@buckhill.co.uk',
             'password' => Hash::make('admin'),
             'is_admin' => UserType::Admin,
             'is_marketing' => MarketingType::NoMarketing,
         ]);

         // Fake users
        User::factory()->count(11)->create([
            'password' => Hash::make('userpassword'),
            'is_admin' => UserType::Default,
        ]);
    }
}
