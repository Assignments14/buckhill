<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class PaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @throws \JsonException
     */
    public function run(): void
    {
        DB::table('payments')->insert([
            'uuid' => uuid_create(),
            'type' => 'credit_card',
            'details' => json_encode([
                'holder_name' => fake()->name,
                'number' => fake()->creditCardNumber,
                'ccy' => fake()->randomNumber(3),
                'expire_date' => Carbon::now()->addYears(3),
            ], JSON_THROW_ON_ERROR),
            'created_at' => Carbon::now(),
        ]);

        DB::table('payments')->insert([
            'uuid' => uuid_create(),
            'type' => 'cash_on_delivery',
            'details' => json_encode([
                'first_name' => fake()->firstName,
                'last_name' => fake()->lastName,
                'address' => fake()->address
            ], JSON_THROW_ON_ERROR),
            'created_at' => Carbon::now(),
        ]);

        DB::table('payments')->insert([
            'uuid' => uuid_create(),
            'type' => 'bank_transfer',
            'details' => json_encode([
                'swift' => fake()->swiftBicNumber,
                'iban' => fake()->iban,
                'name' => fake()->name,
            ], JSON_THROW_ON_ERROR),
            'created_at' => Carbon::now(),
        ]);
    }
}
