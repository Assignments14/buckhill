<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Order</title>
</head>
<body>
    <H4>Here is you order</H4>
    <div>{{ $order->uuid }}</div>

    <H4>Products</H4>
    <ol>
        @foreach($products as $product)
            <li>{{ sprintf("%s: %d", $product['title'], $product['quantity']) }}</li>
        @endforeach
    </ol>
</body>
</html>
