<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ruslan Kladko Test Task Swagger</title>
</head>
<body>
    <div id="swagger-api"></div>
    @vite('resources/js/swagger.js')
</body>
</html>