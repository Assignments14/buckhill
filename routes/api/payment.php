<?php

use App\Http\Controllers\PaymentController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'payments'], static function () {
    Route::get('/', [PaymentController::class, 'index']);
});

Route::group(['prefix' => 'payment'], static function() {
    Route::post('create', [PaymentController::class, 'create']);
    Route::get('{payment:uuid}', [PaymentController::class, 'show']);
    Route::put('{payment:uuid}', [PaymentController::class, 'update']);
    Route::delete('{payment:uuid}', [PaymentController::class, 'destroy']);
});
