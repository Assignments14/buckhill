<?php

use App\Http\Controllers\CategoryController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'categories'], static function () {
    Route::get('/', [CategoryController::class, 'index']);
});

Route::group(['prefix' => 'category'], static function() {
    Route::post('create', [CategoryController::class, 'create']);
    Route::get('{category:uuid}', [CategoryController::class, 'show']);
    Route::put('{category:uuid}', [CategoryController::class, 'update']);
    Route::delete('{category:uuid}', [CategoryController::class, 'destroy']);
});
