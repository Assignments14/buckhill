<?php

use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'user'], static function ($router) {

    Route::get('/', [UserController::class, 'view']);
    Route::delete('/', [UserController::class, 'delete']);
    Route::put('/edit', [UserController::class, 'edit']);

    Route::post('create', [UserController::class, 'create']);

    Route::post('login', [UserController::class, 'login']);
    Route::get('logout', [UserController::class, 'logout']);

    Route::post('forgot-password', [UserController::class, 'forgotPassword']);
    Route::post('reset-password-token', [UserController::class, 'resetPasswordToken']);

});
