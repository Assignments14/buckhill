<?php

use App\Http\Controllers\OrderController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'orders'], static function () {
    Route::get('/', [OrderController::class, 'index']);
    Route::get('/dashboard', [OrderController::class, 'dashboard']);
    Route::get('/shipment-locator', [OrderController::class, 'shipmentLocator']);
});

Route::group(['prefix' => 'order'], static function() {
    Route::post('create', [OrderController::class, 'store']);
    Route::get('{order:uuid}', [OrderController::class, 'show']);
    Route::get('{order:uuid}/download', [OrderController::class, 'download']);
    Route::put('{order:uuid}', [OrderController::class, 'update']);
    Route::delete('{order:uuid}', [OrderController::class, 'destroy']);
});
