<?php

use App\Http\Controllers\BrandController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'brands'], static function () {
    Route::get('/', [BrandController::class, 'index']);
});

Route::group(['prefix' => 'brand'], static function() {
    Route::post('create', [BrandController::class, 'create']);
    Route::get('{brand:uuid}', [BrandController::class, 'show']);
    Route::put('{brand:uuid}', [BrandController::class, 'update']);
    Route::delete('{brand:uuid}', [BrandController::class, 'destroy']);
});
