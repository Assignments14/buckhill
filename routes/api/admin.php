<?php

use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'admin'], static function () {

    Route::post('login', [AdminController::class, 'login']);
    Route::get('logout', [AdminController::class, 'logout']);

    Route::post('create', [AdminController::class, 'create']);

    Route::group(['middleware' => 'admins_only'], static function () {
        Route::get('user-listing', [AdminController::class, 'userListing']);
        Route::put('user-edit/{user:uuid}', [AdminController::class, 'userEdit']);
        Route::delete('user-delete/{user:uuid}', [AdminController::class, 'userDelete']);
    });

});
