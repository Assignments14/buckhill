<?php

use App\Http\Controllers\BrandController;
use App\Http\Controllers\FileController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'file'], static function() {
    Route::post('upload', [FileController::class, 'upload']);
    Route::get('{file:uuid}', [FileController::class, 'read']);
});
