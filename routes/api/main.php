<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\PromotionController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'main'], static function () {
    Route::get('/blog/{post:uuid}', [PostController::class, 'show']);
    Route::get('/blog', [PostController::class, 'index']);
    Route::get('/promotions', [PromotionController::class, 'index']);
});
