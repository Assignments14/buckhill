<?php

use App\Http\Controllers\OrderStatusController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'order-statuses'], static function () {
    Route::get('/', [OrderStatusController::class, 'index']);
});

Route::group(['prefix' => 'order-status'], static function() {
    Route::post('create', [OrderStatusController::class, 'create']);
    Route::get('{order_status:uuid}', [OrderStatusController::class, 'show']);
    Route::put('{order_status:uuid}', [OrderStatusController::class, 'update']);
    Route::delete('{order_status:uuid}', [OrderStatusController::class, 'destroy']);
});
