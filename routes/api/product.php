<?php

use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'products'], static function () {
    Route::get('/', [ProductController::class, 'index']);
});

Route::group(['prefix' => 'product'], static function() {
    Route::post('create', [ProductController::class, 'create']);
    Route::get('{product:uuid}', [ProductController::class, 'show']);
    Route::put('{product:uuid}', [ProductController::class, 'update']);
    Route::delete('{product:uuid}', [ProductController::class, 'destroy']);
});
