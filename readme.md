#### setup:
```bash
--- DB creation ---
$ sudo mysql
> create database psb;
> create user psb identified by 'psb';
> grant all on psb.* to psb;
> create database psb_testing;
> grant all on psb_testing.* to psb;
  
--- Setup the app ---
$ cp .env.dist .env
$ composer install
$ php artisan key:generate
$ php artisan jwt:generate-certs
$ php artisan migrate:refresh --seed

--- Running tests ---
$ php artisan test
```

[Postman Collection](https://www.postman.com/material-geologist-56479507/workspace/test-assignments/request/17351796-6c14934c-6120-45e7-adf2-602a79ee51b6)

[Swagger API](http://buckhill.test/api/v1/swagger)

### Plan to Action

#### Assumptions
- All money fields (amount, fee) is of type int and in stored in cents to avoid money truncations within money manipulation transactions
- Filter-Sorts implemented in two ways: using spatie-query-builder and later with QueryBuilders which is more flexible.

#### Extra points
- [x] Tests provided
    - [x] BrandTest.php
    - [x] FileTest.php
    - [x] AdminTest.php
    - [ ] CategoryTest.php
    - [ ] OrderStatusTest.php
    - [ ] PaymentTest.php
    - [ ] PostTest.php
    - [ ] ProductTest.php
    - [ ] PromotionTest.php
    - [ ] OrderTest.php
- [x] Currency exchange rate (Level 3)
- [x] Notification service (Level 4)
- [ ] Stripe gateway payment integration (Level 4)
- [ ] State Machine for orders (Level 5)
- [ ] PhpInsights scores > 90

#### Users (CRUD)
- [x] API Routes
- [x] INFO  Model and test
- [x] INFO  Factory
- [x] INFO  Migration
- [x] INFO  Seeder
- [x] INFO  Request
- [x] INFO  Request
- [x] INFO  Controller
- [x] | GET|HEAD  | api/v1/user                      |
- [x] | DELETE    | api/v1/user                      |
- [x] | GET|HEAD  | api/v1/user/orders               |
- [x] | POST      | api/v1/user/create               |
- [x] | POST      | api/v1/user/forgot-password      |
- [x] | POST      | api/v1/user/login                |
- [x] | GET|HEAD  | api/v1/user/logout               |
- [x] | POST      | api/v1/user/reset-password-token |
- [x] | PUT       | api/v1/user/edit                 |
 
#### Middlewares (security, injections)
- [x] api:auth jwt:tokens
- [x] admins_only
- [x] user_only
- [ ] anti-injection (by default?)

#### Admin Endpoint
- [x] | POST      | api/v1/admin/create              |
- [x] | POST      | api/v1/admin/login               |
- [x] | GET|HEAD  | api/v1/admin/logout              |
- [x] | GET|HEAD  | api/v1/admin/user-listing        |
- [x] | PUT       | api/v1/admin/user-edit/{uuid}    |
- [x] | DELETE    | api/v1/admin/user-delete/{uuid}  |

#### User endpoint
- [x] login, logout
- [x] password resets
- [x] create, view, edit
- [ ] orders

#### Categories endpoint (CRUD)
- [x] INFO  Model and test [C:\Users\rouse\src\buckhill\app/Models/Category.php] created successfully.
- [x] INFO  Factory [C:\Users\rouse\src\buckhill\database/factories/CategoryFactory.php] created successfully.
- [x] INFO  Migration [C:\Users\rouse\src\buckhill\database\migrations/2023_08_22_085403_create_categories_table.php] created successfully.
- [x] INFO  Seeder [C:\Users\rouse\src\buckhill\database/seeders/CategorySeeder.php] created successfully.
- [x] INFO  Request [C:\Users\rouse\src\buckhill\app/Http/Requests/StoreCategoryRequest.php] created successfully.
- [x] INFO  Request [C:\Users\rouse\src\buckhill\app/Http/Requests/UpdateCategoryRequest.php] created successfully.
- [x] INFO  Controller [C:\Users\rouse\src\buckhill\app/Http/Controllers/CategoryController.php] created successfully.
- [x] | GET|HEAD  | api/v1/categories                |
- [x] | POST      | api/v1/category/create           |
- [x] | PUT|PATCH | api/v1/category/{uuid}           |
- [x] | DELETE    | api/v1/category/{uuid}           |
- [x] | GET|HEAD  | api/v1/category/{uuid}           |

#### Brands endpoint (CRUD)
- [x] API Routes
- [x] INFO  Model and test [C:\Users\rouse\src\buckhill\app/Models/Brand.php] created successfully.
- [x] INFO  Factory [C:\Users\rouse\src\buckhill\database/factories/BrandFactory.php] created successfully.
- [x] INFO  Migration [C:\Users\rouse\src\buckhill\database\migrations/2023_08_22_125930_create_brands_table.php] created successfully.
- [x] INFO  Seeder [C:\Users\rouse\src\buckhill\database/seeders/BrandSeeder.php] created successfully.
- [x] INFO  Request [C:\Users\rouse\src\buckhill\app/Http/Requests/StoreBrandRequest.php] created successfully.
- [x] INFO  Request [C:\Users\rouse\src\buckhill\app/Http/Requests/UpdateBrandRequest.php] created successfully.
- [x] INFO  Controller [C:\Users\rouse\src\buckhill\app/Http/Controllers/BrandController.php] created successfully.
- [x] | GET|HEAD  | api/v1/brands                    |
- [x] | POST      | api/v1/brand/create              |
- [x] | PUT|PATCH | api/v1/brand/{uuid}              |
- [x] | DELETE    | api/v1/brand/{uuid}              |
- [x] | GET|HEAD  | api/v1/brand/{uuid}              |

#### Order Statuses (CURD)
- [x] INFO  Model and test [C:\Users\rouse\src\buckhill\app/Models/OrderStatus.php] created successfully.
- [x] INFO  Factory [C:\Users\rouse\src\buckhill\database/factories/OrderStatusFactory.php] created successfully.
- [x] INFO  Migration [C:\Users\rouse\src\buckhill\database\migrations/2023_08_22_133632_create_order_statuses_table.php] created successfully.
- [x] INFO  Seeder [C:\Users\rouse\src\buckhill\database/seeders/OrderStatusSeeder.php] created successfully.
- [x] INFO  Request [C:\Users\rouse\src\buckhill\app/Http/Requests/StoreOrderStatusRequest.php] created successfully.
- [x] INFO  Request [C:\Users\rouse\src\buckhill\app/Http/Requests/UpdateOrderStatusRequest.php] created successfully.
- [x] INFO  Controller [C:\Users\rouse\src\buckhill\app/Http/Controllers/OrderStatusController.php] created successfully.
- [x] | POST      | api/v1/order-status/create       |
- [x] | PUT|PATCH | api/v1/order-status/{uuid}       |
- [x] | DELETE    | api/v1/order-status/{uuid}       |
- [x] | GET|HEAD  | api/v1/order-status/{uuid}       |
- [x] | GET|HEAD  | api/v1/order-statuses            |

#### Payments (CRUD)
- [x] INFO  Model and test [C:\Users\rouse\src\buckhill\app/Models/Payment.php] created successfully.
- [x] INFO  Factory [C:\Users\rouse\src\buckhill\database/factories/PaymentFactory.php] created successfully.
- [x] INFO  Migration [C:\Users\rouse\src\buckhill\database\migrations/2023_08_22_142643_create_payments_table.php] created successfully.
- [x] INFO  Seeder [C:\Users\rouse\src\buckhill\database/seeders/PaymentSeeder.php] created successfully.
- [x] INFO  Request [C:\Users\rouse\src\buckhill\app/Http/Requests/StorePaymentRequest.php] created successfully.
- [x] INFO  Request [C:\Users\rouse\src\buckhill\app/Http/Requests/UpdatePaymentRequest.php] created successfully.
- [x] INFO  Controller [C:\Users\rouse\src\buckhill\app/Http/Controllers/PaymentController.php] created successfully.
- [x] | GET|HEAD  | api/v1/payments                  |
- [x] | POST      | api/v1/payments/create           |
- [x] | GET|HEAD  | api/v1/payments/{uuid}           |
- [x] | PUT|PATCH | api/v1/payments/{uuid}           |
- [x] | DELETE    | api/v1/payments/{uuid}           |

#### Orders (CRUD)
- [x] INFO  Model and test [C:\Users\rouse\src\buckhill\app/Models/Order.php] created successfully.
- [x] INFO  Factory [C:\Users\rouse\src\buckhill\database/factories/OrderFactory.php] created successfully.
- [x] INFO  Migration [C:\Users\rouse\src\buckhill\database\migrations/2023_08_22_154723_create_orders_table.php] created successfully.
- [x] INFO  Seeder [C:\Users\rouse\src\buckhill\database/seeders/OrderSeeder.php] created successfully.
- [x] INFO  Request [C:\Users\rouse\src\buckhill\app/Http/Requests/OrderDashboardRequest.php] created successfully.
- [x] INFO  Request [C:\Users\rouse\src\buckhill\app/Http/Requests/OrderShippingLocatorRequest.php] created successfully.
- [x] INFO  Request [C:\Users\rouse\src\buckhill\app/Http/Requests/StoreOrderRequest.php] created successfully.
- [x] INFO  Request [C:\Users\rouse\src\buckhill\app/Http/Requests/UpdateOrderRequest.php] created successfully.
- [x] INFO  Controller [C:\Users\rouse\src\buckhill\app/Http/Controllers/OrderController.php] created successfully.
- [x] | POST      | api/v1/order/create              |
- [x] | GET|HEAD  | api/v1/order/{uuid}              |
- [x] | PUT|PATCH | api/v1/order/{uuid}              |
- [x] | DELETE    | api/v1/order/{uuid}              |
- [x] | GET|HEAD  | api/v1/order/{uuid}/download     |
- [x] | GET|HEAD  | api/v1/orders                    |
- [x] | GET|HEAD  | api/v1/orders/dashboard          |
- [x] | GET|HEAD  | api/v1/orders/shipment-locator   |

#### petshop.products (CRUD)
- [x] INFO  Model and test [C:\Users\rouse\src\buckhill\app/Models/Product.php] created successfully.
- [x] INFO  Factory [C:\Users\rouse\src\buckhill\database/factories/ProductFactory.php] created successfully.
- [x] INFO  Migration [C:\Users\rouse\src\buckhill\database\migrations/2023_08_23_073007_create_products_table.php] created successfully.
- [x] INFO  Seeder [C:\Users\rouse\src\buckhill\database/seeders/ProductSeeder.php] created successfully.
- [x] INFO  Request [C:\Users\rouse\src\buckhill\app/Http/Requests/StoreProductRequest.php] created successfully.
- [x] INFO  Request [C:\Users\rouse\src\buckhill\app/Http/Requests/UpdateProductRequest.php] created successfully.
- [x] INFO  Controller [C:\Users\rouse\src\buckhill\app/Http/Controllers/ProductController.php] created successfully.
- [x] | POST      | api/v1/product/create            |
- [x] | PUT|PATCH | api/v1/product/{uuid}            |
- [x] | DELETE    | api/v1/product/{uuid}            |
- [x] | GET|HEAD  | api/v1/product/{uuid}            |
- [x] | GET|HEAD  | api/v1/products                  |

#### petshop.files (CRUD)
- [x] INFO  Model and test [C:\Users\rouse\src\buckhill\app/Models/File.php] created successfully.
- [x] INFO  Migration [C:\Users\rouse\src\buckhill\database\migrations/2023_08_23_091955_create_files_table.php] created successfully.
- [x] INFO  Request [C:\Users\rouse\src\buckhill\app/Http/Requests/StoreFileRequest.php] created successfully.
- [x] INFO  Controller [C:\Users\rouse\src\buckhill\app/Http/Controllers/FileController.php] created successfully.
- [x] | POST      | api/v1/file/upload               |
- [x] | GET|HEAD  | api/v1/file/{uuid}               | 

#### petshop.posts (CRUD)
- [x] INFO  Model and test [C:\Users\rouse\src\buckhill\app/Models/Post.php] created successfully.
- [x] INFO  Factory [C:\Users\rouse\src\buckhill\database/factories/PostFactory.php] created successfully.
- [x] INFO  Migration [C:\Users\rouse\src\buckhill\database\migrations/2023_08_23_113151_create_posts_table.php] created successfully.
- [x] INFO  Seeder [C:\Users\rouse\src\buckhill\database/seeders/PostSeeder.php] created successfully.
- [x] INFO  Request [C:\Users\rouse\src\buckhill\app/Http/Requests/StorePostRequest.php] created successfully.
- [x] INFO  Request [C:\Users\rouse\src\buckhill\app/Http/Requests/UpdatePostRequest.php] created successfully.
- [x] INFO  Controller [C:\Users\rouse\src\buckhill\app/Http/Controllers/PostController.php] created successfully.

#### petshop.promotions (CRUD)
- [x] INFO  Model and test [C:\Users\rouse\src\buckhill\app/Models/Promotion.php] created successfully.
- [x] INFO  Factory [C:\Users\rouse\src\buckhill\database/factories/PromotionFactory.php] created successfully.
- [x] INFO  Migration [C:\Users\rouse\src\buckhill\database\migrations/2023_08_23_115319_create_promotions_table.php] created successfully.
- [x] INFO  Seeder [C:\Users\rouse\src\buckhill\database/seeders/PromotionSeeder.php] created successfully.

#### Main page endpoint
- [x] | GET|HEAD  | api/v1/main/blog                 |
- [x] | GET|HEAD  | api/v1/main/blog/{uuid}          |
- [x] | GET|HEAD  | api/v1/main/promotions           |

#### JWT Tokens

##### developer shortcuts:
- php artisan make:model Category -cfmsrR --pest
- php artisan make:model Category -fms --pest 
- php artisan migrate:refresh --seed
- php artisan db:seed --class=UserTableSeeder
- -dxdebug.mode=debug -dxdebug.client_host=127.0.0.1 -dxdebug.client_port=9003 -dxdebug.start_with_request=yes


#### Support
    OPENAI_KEY=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
    aicomiits

#### Progress and Questions:
1. fixRange filter meaning (monthly)
2. page and limit params (confusion?)
3. filter-sorts has 2 implementations: Spatie component and internal QueryBuilder helper/trait
4. best model implementation -- Order
5. Actions, FormRequests, QueryBuilders, Middlewares, 
6. It wasn't possible to consume all the requirements once at all (they are spread without links), so implementation advances to the end of the task.
7. 