<?php

namespace Tests\Feature\Models;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class FileTest extends TestCase
{
    public const UPLOAD_FOLDER = '/app/uploads/';

    public const UPLOAD_FILE = 'avatar.jpg';

    /**
     * A basic feature test example.
     */
    public function test_file_can_be_uploaded(): void
    {
        $file = UploadedFile::fake()->image(self::UPLOAD_FILE);

        $response = $this->post('/file/upload', [
            'file' => $file,
        ]);

        $response
            ->assertStatus(200)
            ->assertJsonPath('success', 1)
            ->assertJsonPath('data.name', self::UPLOAD_FILE)
            ->assertJsonPath('data.type', 'image/jpeg');

        $fileOnDisk = storage_path() . self::UPLOAD_FOLDER . $file->hashName();
        $this->fileExists($fileOnDisk);
    }

    public function test_can_download_uploaded_file()
    {
        $file = UploadedFile::fake()->image(self::UPLOAD_FILE);

        $response = $this->post('/file/upload', [
            'file' => $file,
        ]);

        $response->assertStatus(200);

        $file_uuid = $response->decodeResponseJson()['data']['uuid'];

        $response = $this->getJson("api/v1/file/$file_uuid");

        $response->assertStatus(200);

    }
}
