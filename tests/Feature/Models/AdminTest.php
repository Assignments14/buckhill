<?php

namespace Tests\Feature\Models;

use Database\Seeders\UserSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AdminTest extends TestCase
{
    use RefreshDatabase;

    public const ADMIN_CREDENTIALS = [
        'email' => 'admin@buckhill.co.uk',
        'password' => 'admin',
    ];

    public const BEARER_TOKEN = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJpc3MiOiJodHRwOi8vYnVja2hpbGwudGVzdC9hcGkvdjEvYWRtaW4vbG9naW4iLCJpYXQiOjE2OTI5NTU5NDcsImV4cCI6MTY5MzU2MDc0NywibmJmIjoxNjkyOTU1OTQ3LCJqdGkiOiJ0b2ZXS3paSVY5TUxXaktDIiwic3ViIjoiMSIsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjciLCIwIjoidXVpZCIsInV1aWQiOm51bGx9.nGFs5GSPlQO-vCfZs3FpOvlazAXU1wApS2Jlmaez4lgIemiHhb0ZvgSKvUSncJlAcZoPxwhcbQYYjyyvoNM7m_3t8oCHU5fW9dce_3M-E7CJ8-bCFheOhS3r3ZewbhwInAdpu1hwkL3hgigDsXBVqYZIAWlFViNC9lXaBkG_b66c8ee719JTYsFLpWxKaF1U8JB1WT3bTidRnhTxuQD2FI3i63FwCrNSwMkE9-lHJQiBZEHmOrLG0W-Q2LCU5igr8weiNCqyvHwa3-aDGQ4QFJTzscZWOxzinnsraU_vHEwhl0OypprhPJOdbO_qDTxM0JzIQ4punvVHVTGQIzKG8q54asVcmi3o4Xn62qpiLSOHg1GVPkR88WeBJRhQqmpQvkNKtCOhrfTpWRm-YtgUQBQTyF5Rm4v9XWtOy79S--_EsXDE0KgexvBiknPiLsf49d46z-I9YlycXzJhO6IE_bC7Gzu-Bapo6pT176Ci02KsaXFLqPfF9Ai6zFhjXSOQtlHR6TJ9E5WS_3oJA08GZCzD7L0uZqyZShxydhFg8fbfJ5l6US0a6ieCGH4cCeEs4aRxn4lf-Qh-pSYVHJIIBtTckKkgWdBYrPh8jtAGJrgTAByulDoDv-wLorhRkS5GteuVPnwNXqJ_awvp4Lv9BdBcJU-qI2ksgpeN0O2OChw';

    public function apiHeaders(): array
    {
        return [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . self::BEARER_TOKEN,
        ];
    }

    public function afterRefreshingDatabase(): void
    {
        $this->seed(UserSeeder::class);
    }

    /**
     * A basic feature test example.
     */
    public function test_admin_can_login(): void
    {
        $response = $this->post('/admin/login', self::ADMIN_CREDENTIALS);

//        $response->dumpHeaders();
//        $response->dump();

        $response
            ->assertStatus(200)
            ->assertJsonPath('success', 1)
            ->assertJsonStructure([
                'success',
                'data' => [
                    'token'
                ],
                'error',
                'errors',
                'extra',
            ]);

    }

    public function test_admin_can_logout_with_token(): void
    {
        $response = $this->withHeaders($this->apiHeaders())
            ->post('/admin/login', [
            'email' => 'admin@buckhill.co.uk',
            'password' => 'admin',
        ]);

        $response->assertStatus(200);

        $response = $this->withHeaders($this->apiHeaders())->get('/admin/logout');

        $response->assertStatus(404);
    }

    public function test_admin_cannot_logout_wo_token(): void
    {
        $response = $this->post('/admin/login', [
            'email' => 'admin@buckhill.co.uk',
            'password' => 'admin',
        ]);

        $response->assertStatus(200);

        $response = $this->get('/admin/logout');

        $response->assertStatus(404);
    }

    /*
     *
     */
    public function test_admin_list_only_users(): void
    {
        $this->markTestIncomplete('Waiting for Master...');

        /*
         * Admin
         */
        $params = [
            'email' => self::ADMIN_CREDENTIALS['email'],
        ];

        $response = $this->withHeaders($this->apiHeaders())->get('/admin/user-listing?' . http_build_query($params));

        $response->assertStatus(200)
            ->assertJsonCount(0, 'data');

        /*
         * User
         */
        $params = [
            'email' => 'christophe.ward@gmail.com',
        ];

        $response = $this->withHeaders($this->apiHeaders())->get('/admin/user-listing?' . http_build_query($params));

        $response->assertStatus(200)
            ->assertJsonCount(1, 'data');

    }

}
