<?php

namespace Tests\Feature\Models;

use Database\Seeders\BrandSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BrandTest extends TestCase
{
    use RefreshDatabase;

    public function afterRefreshingDatabase(): void
    {
    }

    public function test_brand_get(): void
    {
        $response = $this->postJson('brand/create', [
            'title' => 'Brand new',
        ]);

        $uuid = $response->decodeResponseJson()['data']['uuid'];

        $response = $this->getJson("api/v1/brand/$uuid");

        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => 1,
                'data' => [
                    'title' => 'Brand new',
                ],
            ]);
    }

    public function test_brands_list(): void
    {
        $this->seed(BrandSeeder::class);

        $response = $this->getJson('brands');

        $response
            ->assertStatus(200)
            ->assertJsonPath('success', 1)
            ->assertJsonStructure([
                'success',
                'data',
                'error',
                'errors',
                'extra',
            ]);

        $data = $response->decodeResponseJson()['data'];
        $this->assertNotEmpty($data);
    }

    public function test_brands_list_with_limit(): void
    {
        $this->seed(BrandSeeder::class);

        $params = [
            'page' => '1',
            'limit' => '7',
            'sortBy' => 'title',
            'desc' => true,
        ];

        $response = $this->get('brands?'. http_build_query($params));

        $response
            ->assertStatus(200)
            ->assertJsonPath('success', 1)
            ->assertJsonStructure([
                'success',
                'data',
                'error',
                'errors',
                'extra',
            ])
            ->assertJsonCount(7, 'data');
    }

    /**
     * A basic feature test example.
     */
    public function test_brand_create(): void
    {
        $response = $this->postJson('brand/create', [
            'title' => 'Brand new',
        ]);

        $response
            ->assertStatus(201)
            ->assertJson([
                'success' => 1,
            ]);
    }

    /**
     * @throws \Throwable
     */
    public function test_brand_update(): void
    {
        $response = $this->postJson('brand/create', [
            'title' => 'Brand new',
        ]);

        $uuid = $response->decodeResponseJson()['data']['uuid'];

        $response = $this->putJson("api/v1/brand/$uuid", [
            'title' => 'Brand updated',
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => 1,
                'data' => [
                    'title' => 'Brand updated',
                ],
            ]);
    }

    public function test_brand_delete(): void
    {
        $response = $this->postJson('brand/create', [
            'title' => 'Brand new',
        ]);

        $uuid = $response->decodeResponseJson()['data']['uuid'];

        $response = $this->delete("api/v1/brand/$uuid");

        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => 1,
            ]);
    }
}
