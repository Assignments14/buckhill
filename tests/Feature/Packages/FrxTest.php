<?php

namespace Tests\Feature\Packages;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class FrxTest extends TestCase
{
    /**
     * A basic feature test example.
     */
    public function test_can_exchange_eur(): void
    {
//        $this->markTestIncomplete('Waiting for Master...');

        $params = [
            'amount' => 100,
        ];

        $response = $this->get('/frx?' . http_build_query($params));

        $response->assertStatus(200)
            ->assertJsonPath('success', 1)
            ->assertJsonPath('data.exchange', 100);
    }

    public function test_can_exchange_non_eur(): void
    {
//        $this->markTestIncomplete('Waiting for Master...');

        $params = [
            'amount' => 100,
            'ccy' => 'GBP',
        ];

        $response = $this->get('/frx?' . http_build_query($params));

        $response->assertStatus(200)
            ->assertJsonPath('success', 1);
    }

    public function test_error_when_exchange_not_listed_ccy(): void
    {
//        $this->markTestIncomplete('Waiting for Master...');

        $params = [
            'amount' => 100,
            'ccy' => 'UAH',
        ];

        $response = $this->get('/frx?' . http_build_query($params));

        $response->assertStatus(Response::HTTP_EXPECTATION_FAILED)
            ->assertJsonPath('success', 0);
    }
}
