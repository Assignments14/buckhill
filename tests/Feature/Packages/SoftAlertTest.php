<?php

namespace Tests\Feature\Packages;

use App\Models\Order;
use Buckhill\SoftAlert\Events\OrderStatusChanged;
use Buckhill\SoftAlert\Listeners\SendTeamNotification;
use Database\Seeders\BrandSeeder;
use Database\Seeders\CategorySeeder;
use Database\Seeders\FileSeeder;
use Database\Seeders\OrderStatusSeeder;
use Database\Seeders\PaymentSeeder;
use Database\Seeders\ProductSeeder;
use Database\Seeders\UserSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class SoftAlertTest extends TestCase
{
    use RefreshDatabase;

    protected function afterRefreshingDatabase(): void
    {
        $this->artisan('db:seed');
    }

    /**
     * A basic feature test example.
     */
    public function test_event_dispatched_when_order_status_changed(): void
    {

//        $this->markTestIncomplete('Waiting for Master...');

        Event::fake([
            OrderStatusChanged::class
        ]);

        $order = Order::factory()->create(['order_status_id' => 1]);
        $order->order_status_id = 3;
        $order->save();

        Event::assertDispatched(OrderStatusChanged::class);

    }

}
