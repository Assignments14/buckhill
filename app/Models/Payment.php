<?php

namespace App\Models;

use App\Support\HasJson;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Payment extends Model
{
    use HasFactory, HasUuids, HasJson;

    protected $fillable = ['type', 'details'];

    protected $casts = [
        'details' => 'array',
    ];

    public function uniqueIds(): array
    {
        return ['uuid'];
    }

    protected function details(): Attribute
    {
        return Attribute::make(
            set: static fn (string $value) => $this->castJson($value),
        );
    }

    public function order(): BelongsTo
    {
        return $this->belongsTo(Order::class);
    }
}
