<?php

namespace App\Models;

use App\Models\QueryBuilders\BrandsListQueryBuilder;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    use HasFactory, HasUuids;

    protected $fillable = ['title', 'slug'];

    public function uniqueIds(): array
    {
        return ['uuid'];
    }

    public function newEloquentBuilder($query): BrandsListQueryBuilder
    {
        return new BrandsListQueryBuilder($query);
    }
}
