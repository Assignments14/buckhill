<?php

namespace App\Models;

use App\Models\QueryBuilders\OrderQueryBuilder;
use App\Support\HasJson;
use Buckhill\Progressor\Steppable;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Order extends Model
{
    use HasFactory, HasUuids, HasJson, Steppable;

    protected $fillable = [
        'products', 'address', 'delivery_fee', 'amount',
    ];

    protected $casts = [
        'products' => 'json',
        'address' => 'json',
    ];

    public function uniqueIds(): array
    {
        return ['uuid'];
    }

    protected function deliveryFee(): Attribute
    {
        return Attribute::make(
            get: static fn ($value) => (float) ($value / 100),
            set: static fn ($value) => (int) ($value * 100),
        );
    }

    protected function amount(): Attribute
    {
        return Attribute::make(
            get: static fn ($value) => (float) ($value / 100),
            set: static fn ($value) => (int) ($value * 100),
        );
    }

    public function newEloquentBuilder($query): OrderQueryBuilder
    {
        return new OrderQueryBuilder($query);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function status(): HasOne
    {
        return $this->hasOne(OrderStatus::class, 'id', 'order_status_id');
    }

    public function payment(): HasOne
    {
        return $this->hasOne(Payment::class);
    }
}
