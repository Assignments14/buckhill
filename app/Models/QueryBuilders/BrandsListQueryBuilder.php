<?php

namespace App\Models\QueryBuilders;

use App\Models\QueryBuilders\FilterSortsQueryBuilder;

class BrandsListQueryBuilder extends FilterSortsQueryBuilder
{
    public function baseFilterSortsFromRequest(array $validated): self
    {
        $query = parent::baseFilterSortsFromRequest($validated);

        if (array_key_exists('title', $validated)) {
            $query->where('title', 'like', $this->likeAny($validated['title']));
        }

        return $query;
    }
}