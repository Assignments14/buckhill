<?php

namespace App\Models\QueryBuilders;

use App\Enums\UserType;
use Illuminate\Database\Eloquent\Builder;

class UserQueryBuilder extends FilterSortsQueryBuilder
{
    public function whereUserTypeIs(UserType $userType): self
    {
        return $this->whereIsAdmin($userType->value);
    }

    public function whereUser(): self
    {
        return $this->whereUserTypeIs(UserType::Default);
    }

    public function baseFilterSortsFromRequest(array $validated): self
    {
        $query = parent::baseFilterSortsFromRequest($validated);

        $query->where('is_admin', 0);

        if (array_key_exists('email', $validated)) {
            $query->where('email', '=', $validated['email']);
        }

        if (array_key_exists('first_name', $validated)) {
            $query->where('lower(first_name)', '=', strtolower($validated['first_name']));
        }

        return $query;
    }

}