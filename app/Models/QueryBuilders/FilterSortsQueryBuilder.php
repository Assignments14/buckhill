<?php

namespace App\Models\QueryBuilders;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class FilterSortsQueryBuilder extends Builder
{
    protected function likeAny(string $search): string
    {
        return '%' . $search . '%';
    }

    public function baseFilterSortsFromRequest(array $validated): self
    {
        if (array_key_exists('page', $validated)) {
            $page = $validated['page'];
            $skip = env('APP_DEFAULT_PER_PAGE', 15) * ($page-1);
            $this->skip($skip);
        }

        if (array_key_exists('limit', $validated)) {
            $this->limit($validated['limit']);
        }

        if (array_key_exists('sortBy', $validated)) {
            $this->orderBy($validated['sortBy'], ($validated['desc']) ? 'desc' : 'asc');
        }

        if (array_key_exists('dateRange', $validated)) {
            $from = $validated['dateRange']['from'];
            $to = $validated['dateRange']['to'];
            $this->whereBetween('orders.created_at', [$from, $to]);
        }

        return $this;
    }
}