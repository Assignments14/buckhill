<?php

namespace App\Models\QueryBuilders;

use App\Models\Product;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;

class OrderQueryBuilder extends FilterSortsQueryBuilder
{
    public function createdBetween(\DateTime $from, \DateTime $to): self
    {
        return $this->whereBetween('created_at', [$from, $to]);
    }

    public function selectForDashboard(): self
    {
        return $this->select(
            ['orders.uuid', 'users.uuid as customer_uuid', 'products', 'orders.address',
                'delivery_fee', 'amount', 'orders.created_at', 'shipped_at'
            ]
        );
    }

    public function selectForShipment(): self
    {
        return $this->select(
            ['orders.uuid', 'users.uuid as customer_uuid', 'products', 'orders.address',
                'delivery_fee', 'amount', 'orders.created_at', 'shipped_at'
            ]
        );
    }

    public function withUser(): self
    {
        return $this->leftJoin('users', 'orders.user_id', '=', 'users.id');
    }

    public function whereOrderUuid(string $uuid): self
    {
        return $this->where('users.uuid', $uuid);
    }

    public function whereCustomerUuid(string $uuid): self
    {
        return $this->where('users.uuid', $uuid);
    }

    public function baseFilterSortsFromRequest(array $validated): self
    {
        $query = parent::baseFilterSortsFromRequest($validated);

        if (array_key_exists('orderUuid', $validated)) {
            $query->whereOrderUuid($validated['orderUuid']);
        }

        if (array_key_exists('customerUuid', $validated)) {
            $query->whereCustomerUuid($validated['orderUuid']);
        }

        return $query;
    }

    public function populateProducts(array $order_products): array
    {
        $query_in = array_column($order_products, 'product');
        $products = Product::whereIn('uuid', $query_in)->get()->toArray();
        $products = Arr::mapWithKeys($products, static function (array $item, int $key) {
            return [$item['uuid'] => $item['title']];
        });

        return Arr::map($order_products, static function(array $value, int $key) use ($products) {
            return [
                'uuid' => $value['product'],
                'title' => $products[$value['product']],
                'quantity' => $value['quantity'],
            ];
        });

    }
}