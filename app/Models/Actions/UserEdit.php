<?php

namespace App\Models\Actions;

use App\Models\User;

class UserEdit
{
    public function __invoke(User $user, $validated_data)
    {
        $user->update($validated_data);
    }
}