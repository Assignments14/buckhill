<?php

namespace App\Models\Actions;

use App\Http\Requests\PasswordResetRequest;
use App\Models\User;
use App\Support\CustomApiResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

class ResetPassword
{
    use CustomApiResponse;

    public function __invoke(PasswordResetRequest $request): JsonResponse
    {
        $user = User::whereEmail($request->only('email'))->first();
        if (!$user) {
            return $this->responseError('User not found', Response::HTTP_NOT_FOUND);
        }

        $token = $request->only('token');
        $passwordBroker = app('auth.password.broker');
        if (! $passwordBroker->tokenExists($user, $token['token']) ) {
            return $this->responseError('Token does not exist', Response::HTTP_EXPECTATION_FAILED);
        }

        $credentials = $request->only(['email', 'token', 'password']);

        $passwordBroker->reset($credentials, function ($user, $password) {
            $user->password = Hash::make($password);
            $user->save();
        });

        return $this->responseSuccess();
    }
}