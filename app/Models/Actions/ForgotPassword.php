<?php

namespace App\Models\Actions;

use App\Models\User;
use App\Support\CustomApiResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class ForgotPassword
{
    use CustomApiResponse;

    public function __invoke(Request $request): JsonResponse
    {
        $request->validate(['email' => 'required|email']);

        $email = $request->only('email');

        $user = User::whereEmail($email)->first();
        if (! $user) {
            return $this->responseError('User not found', Response::HTTP_NOT_FOUND);
        }

        $prevToken = DB::table(config('auth.passwords.users.table'))->whereEmail($email)->first();
        if ($prevToken?->token) {
            return $this->responseError('Reset password token already issued', Response::HTTP_NOT_ACCEPTABLE);
        }

        $passwordBroker = app('auth.password.broker');

        $token = $passwordBroker->createToken($user);

        return $this->responseSuccessWithData(compact('token'));
    }
}