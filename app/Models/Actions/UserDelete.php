<?php

namespace App\Models\Actions;

use App\Models\User;

class UserDelete
{
    public function __invoke(User $user)
    {
        $user->delete();
    }
}