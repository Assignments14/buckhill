<?php

namespace App\Models\Actions;

use Carbon\Carbon;

class UpdateLastLogin
{
    public function __invoke(): void
    {
        $user = auth()->user();
        if ($user) {
            $user->last_login_at = Carbon::now();
            $user->save();
        }
    }
}