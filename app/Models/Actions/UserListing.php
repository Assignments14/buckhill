<?php

namespace App\Models\Actions;

use App\Enums\UserType;
use App\Models\User;
use Spatie\QueryBuilder\QueryBuilder;

class UserListing
{
    public function __invoke(): array
    {
        return QueryBuilder::for(User::whereUser())
            ->allowedFilters(['first_name', 'email', 'phone_number', 'address', 'created_at'])
            ->allowedSorts(['first_name', 'email', 'phone_number', 'address', 'created_at'])
            ->whereIsAdmin(UserType::Default)
            ->simplePaginate()->items();
    }
}