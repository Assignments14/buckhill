<?php

namespace App\Models\Actions;

use App\Http\Requests\CreateUserRequest;
use App\Models\User;
use App\Support\CustomApiResponse;
use Illuminate\Http\JsonResponse;

class EditUser
{
    use CustomApiResponse;

    public function __invoke(CreateUserRequest $request): JsonResponse
    {
        $validated = $request->validated();

        auth()->user()?->update($validated);

        return $this->responseSuccess();
    }
}