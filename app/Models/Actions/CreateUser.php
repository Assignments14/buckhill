<?php

namespace App\Models\Actions;

use App\Enums\UserType;
use App\Http\Requests\CreateUserRequest;
use App\Models\User;
use App\Support\CustomApiResponse;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class CreateUser
{
    use CustomApiResponse;

    public function __invoke(CreateUserRequest $request, UserType $userType): JsonResponse
    {
        $validated = $request->validated();
        $uuid = uuid_create();
        $validated['uuid'] = $uuid;
        $validated['is_admin'] = $userType->value;
        User::create($validated);

        $token = auth()->claims(compact('uuid'))->attempt($request->only('email', 'password'));
        if (! $token ) {
            return $this->responseError('Failed to authenticate newly created user', Response::HTTP_EXPECTATION_FAILED);
        }

        return $this->responseSuccessWithData(compact('token'));
    }
}