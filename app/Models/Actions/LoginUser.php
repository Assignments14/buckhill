<?php

namespace App\Models\Actions;

use App\Support\CustomApiResponse;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class LoginUser
{
    use CustomApiResponse;

    public function __invoke(): JsonResponse
    {
        $credentials = request(['email', 'password']);
        $uuid = request('uuid');
        $token = auth()->claims(compact('uuid'))->attempt($credentials);
        if (! $token) {
            return $this->responseError('Failed to authenticate user', Response::HTTP_UNAUTHORIZED);
        }

        // todo: consider refactor to event
        (new UpdateLastLogin())();

        return $this->responseSuccessWithData(compact('token'));
    }
}