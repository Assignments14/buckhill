<?php

namespace App\Models\Actions;

use App\Http\Requests\StoreFileRequest;
use App\Models\File;

class FileUpload
{
    public function __invoke(StoreFileRequest $request): array
    {
        $file = $request->file('file');

        $name = $file->getClientOriginalName();
        $path = $file->store('uploads');
        $size = $file->getSize();
        $type = $file->getMimeType();

        $saved = File::create(compact('name', 'path', 'size', 'type'));

        return $saved->fresh()->toArray();

    }
}