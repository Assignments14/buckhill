<?php

namespace App\Models\DTO;

use App\Http\Requests\StoreProductRequest;

class ProductData
{
    public function __construct(
        public string $category_uuid,
        public string $title,
        public string $description,
        public int $price,
        public array $metadata,
    )
    {

    }

    public function toArray(): array
    {
        return (array) $this;
    }

    public static function fromRequest(StoreProductRequest $request): self
    {
        return new self(
            category_uuid: $request->get('category_uuid'),
            title: $request->get('title'),
            description: $request->get('description'),
            price: $request->get('price') * 100,
            metadata: [
                'brand' => $request->get('brand'),
                'image' => $request->get('image'),
            ]
        );
    }
}