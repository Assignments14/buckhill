<?php

namespace App\Exceptions;

use App\Support\ApiResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class BuckhillValidation extends ValidationException
{
    public function render($request): JsonResponse
    {
        return new JsonResponse(
            ApiResponse::error(
                'Failed Validation',
                $this->validator->errors()->getMessages()
            ),
//            Response::HTTP_UNPROCESSABLE_ENTITY
            Response::HTTP_PRECONDITION_REQUIRED
        );
    }
}
