<?php

namespace App\Http\Middleware;

use App\Support\ApiResponse;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminsOnly
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $token = $request->bearerToken();
        $user = auth()->setToken($token)->user();

        if (! $user) {
            return response()->json(ApiResponse::error('Unknown user'), Response::HTTP_BAD_REQUEST);
        }

        if (! $user->isAdmin()) {
            return response()->json(ApiResponse::error('Admin Only Area'), Response::HTTP_FORBIDDEN);
        }

        return $next($request);
    }
}
