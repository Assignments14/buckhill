<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Schema;
use Illuminate\Validation\Rule;

class OrderDashboardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        // todo: populate for all filtersort requests
        return [
            'page' => 'integer',
            'limit' => 'integer',
            'sortBy' => [
                    Rule::in(Schema::getColumnListing('orders')),
                ],
            'desc' => 'boolean',
            'dateRange' => 'array|keys_in:from,to',
        ];
    }
}
