<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'category_uuid' => 'required|exists:categories,uuid',
            'title' => 'required|max:255',
            'price' => 'required|decimal:2',
            'description' => 'required',
            'image' => 'required|exists:brands,uuid',
            'brand' => 'required|exists:files,uuid',
        ];
    }
}
