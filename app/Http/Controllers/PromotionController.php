<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Models\Promotion;
use App\Support\CustomApiResponse;
use App\Support\FilterSortRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;

class PromotionController extends ApiController
{
    use CustomApiResponse, FilterSortRequests;

    protected array $allowedFilters = [];

    protected array $allowedSorts = ['title', 'created_at'];

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): JsonResponse
    {
        $filteredSorted = $this->indexWithFilterSort($request, Promotion::class);
        return $this->responseSuccessWithData($filteredSorted);
    }

}
