<?php

namespace App\Http\Controllers;

use App\Enums\UserType;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\PasswordResetRequest;
use App\Models\Actions\CreateUser;
use App\Models\Actions\EditUser;
use App\Models\Actions\ForgotPassword;
use App\Models\Actions\LoginUser;
use App\Models\Actions\ResetPassword;
use App\Models\Actions\UpdateLastLogin;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Js;
use Symfony\Component\HttpFoundation\Response;

class UserController extends ApiController
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'create']]);
    }

    public function login(): JsonResponse
    {
        return (new LoginUser())();
    }

    public function logout(): JsonResponse
    {
        auth()->logout();
        return $this->responseSuccess();
    }

    public function forgotPassword(Request $request): JsonResponse
    {
        return (new ForgotPassword())($request);
    }

    public function resetPasswordToken(PasswordResetRequest $request): JsonResponse
    {
        return (new ResetPassword())($request);
    }

    public function create(CreateUserRequest $request): JsonResponse
    {
        return (new CreateUser())($request, UserType::Default);
    }

    public function view(): JsonResponse
    {
        return $this->responseSuccessWithData(auth()->user()?->toArray());
    }

    public function edit(CreateUserRequest $request): JsonResponse
    {
        (new EditUser())($request);
        return $this->responseSuccess();
    }

    public function delete()
    {
        auth()->user()?->delete();
        return $this->responseSuccess();
    }
}
