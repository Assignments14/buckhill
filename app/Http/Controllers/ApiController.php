<?php

namespace App\Http\Controllers;

use App\Support\ApiResponse;
use App\Support\CustomApiResponse;
use Illuminate\Http\JsonResponse;

class ApiController extends Controller
{
    use CustomApiResponse;
}