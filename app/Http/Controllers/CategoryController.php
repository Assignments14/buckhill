<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Http\Requests\StoreCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\Support\CustomApiResponse;
use App\Support\FilterSortRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;

class CategoryController extends Controller
{
    use CustomApiResponse, FilterSortRequests;

    protected array $allowedFilters = ['title'];

    protected array $allowedSorts = ['title', 'created_at'];

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): JsonResponse
    {
        $filteredSorted = $this->indexWithFilterSort($request, Category::class);
        return $this->responseSuccessWithData($filteredSorted);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function create(StoreCategoryRequest $request): JsonResponse
    {
        $category = Category::create($request->validated());
        return $this->responseSuccessWithData($category->fresh()->toArray());
    }

    /**
     * Display the specified resource.
     */
    public function show(Category $category): JsonResponse
    {
        return $this->responseSuccessWithData($category->toArray());
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateCategoryRequest $request, Category $category): JsonResponse
    {
        $category->update($request->validated());
        return $this->responseSuccessWithData($category->toArray());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Category $category): JsonResponse
    {
        $category->delete();
        return $this->responseSuccess();
    }
}
