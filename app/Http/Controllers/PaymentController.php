<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use App\Http\Requests\StorePaymentRequest;
use App\Http\Requests\UpdatePaymentRequest;
use App\Support\CustomApiResponse;
use App\Support\FilterSortRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Js;
use Spatie\QueryBuilder\QueryBuilder;

class PaymentController extends Controller
{
    use CustomApiResponse, FilterSortRequests;

    protected array $allowedFilters = [];

    protected array $allowedSorts = ['title', 'created_at'];

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): JsonResponse
    {
        $filteredSorted = $this->indexWithFilterSort($request, Payment::class);
        return $this->responseSuccessWithData($filteredSorted);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function create(StorePaymentRequest $request): JsonResponse
    {
        $payment = Payment::create($request->validated());
        return $this->responseSuccessWithData($payment->fresh()->toArray());
    }

    /**
     * Display the specified resource.
     */
    public function show(Payment $payment): JsonResponse
    {
        return $this->responseSuccessWithData($payment->toArray());
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdatePaymentRequest $request, Payment $payment): JsonResponse
    {
        $payment->update($request->validated());
        return $this->responseSuccessWithData($payment->toArray());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Payment $payment): JsonResponse
    {
        $payment->delete();
        return $this->responseSuccess();
    }
}
