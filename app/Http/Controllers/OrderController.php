<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderDashboardRequest;
use App\Http\Requests\OrderShippingLocatorRequest;
use App\Models\Order;
use App\Http\Requests\StoreOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Models\Product;
use App\Support\CustomApiResponse;
use App\Support\FilterSortRequests;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class OrderController extends ApiController
{
    use CustomApiResponse, FilterSortRequests;

    /**
     * Display a listing of the resource.
     */
    public function index(OrderDashboardRequest $request): JsonResponse
    {
        $filteredSorted = $this->indexWithFilterSort($request, Order::class);
        return $this->responseSuccessWithData($filteredSorted);
    }

    public function dashboard(OrderDashboardRequest $request): JsonResponse
    {
        $validated = $request->validated();

        $query = Order::leftJoin('users', 'orders.user_id', '=', 'users.id')
            ->baseFilterSortsFromRequest($validated)
            ->selectForShipment();

        return $this->responseSuccessWithData($query->get()->toArray());
    }

    public function shipmentLocator(OrderShippingLocatorRequest $request): JsonResponse
    {
        $validated = $request->validated();

        $query = Order::withUser()
            ->baseFilterSortsFromRequest($validated)
            ->selectForShipment();

        return $this->responseSuccessWithData($query->get()->toArray());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreOrderRequest $request): JsonResponse
    {
        $model = Order::create($request->validated());
        return $this->responseSuccessWithData($model->fresh()->toArray());
    }

    /**
     * Display the specified resource.
     */
    public function show(Order $order): JsonResponse
    {
        return $this->responseSuccessWithData($order->toArray());
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateOrderRequest $request, Order $order): JsonResponse
    {
        $order->update($request->validated());
        return $this->responseSuccessWithData($order->toArray());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Order $order): JsonResponse
    {
        $order->delete();
        return $this->responseSuccess();
    }

    /**
     * @throws \JsonException
     */
    public function download(Order $order): Response
    {
        $products = Order::populateProducts($order->products);

        header("Content-type:application/pdf");

        return PDF::loadView('order-pdf', compact('order', 'products'))
            ->download(sprintf("order_%s.pdf", $order->uuid));
    }
}
