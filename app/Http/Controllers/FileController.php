<?php

namespace App\Http\Controllers;

use App\Models\Actions\FileUpload;
use App\Models\File;
use App\Http\Requests\StoreFileRequest;
use App\Http\Requests\UpdateFileRequest;
use App\Support\CustomApiResponse;
use http\Env\Response;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class FileController extends Controller
{

    use CustomApiResponse;

    /**
     * Store a newly created resource in storage.
     */
    public function upload(StoreFileRequest $request): JsonResponse
    {
        $result = (new FileUpload())($request);
        return $this->responseSuccessWithData($result);
    }

    /**
     * Display the specified resource.
     */
    public function read(File $file): BinaryFileResponse
    {
        $path = storage_path('app/' . $file->path);
        return response()->download($path);
    }

}
