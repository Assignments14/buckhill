<?php

namespace App\Http\Controllers;

use App\Models\DTO\ProductData;
use App\Models\Product;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Support\CustomApiResponse;
use App\Support\FilterSortRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;

class ProductController extends ApiController
{
    use CustomApiResponse, FilterSortRequests;

    protected array $allowedFilters = ['title'];

    protected array $allowedSorts = ['title', 'brand'];

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): JsonResponse
    {
        $filteredSorted = $this->indexWithFilterSort($request, Product::class);
        return $this->responseSuccessWithData($filteredSorted);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function create(StoreProductRequest $request): JsonResponse
    {
        // todo: populate on all models
        $validated = ProductData::fromRequest($request);
        $product = Product::create($validated->toArray());
        return $this->responseSuccessWithData($product->fresh()->toArray());
    }

    /**
     * Display the specified resource.
     */
    public function show(Product $product): JsonResponse
    {
        return $this->responseSuccessWithData($product->toArray());
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateProductRequest $request, Product $product): JsonResponse
    {
        $product->update($request->validated());
        return $this->responseSuccessWithData($product->toArray());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Product $product): JsonResponse
    {
        $product->delete();
        return $this->responseSuccess();
    }
}
