<?php

namespace App\Http\Controllers;

use App\Enums\UserType;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UserListingRequest;
use App\Models\Actions\CreateUser;
use App\Models\Actions\LoginUser;
use App\Models\Actions\UpdateLastLogin;
use App\Models\Actions\UserDelete;
use App\Models\Actions\UserEdit;
use App\Models\Actions\UserListing;
use App\Models\User;
use App\Support\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;

final class AdminController extends ApiController
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'create']]);
    }

    public function login(): JsonResponse
    {
        return (new LoginUser())();
    }

    public function logout(): JsonResponse
    {
        auth()->logout();
        return $this->responseSuccess();
    }

    public function create(CreateUserRequest $request): JsonResponse
    {
        return (new CreateUser())($request, UserType::Admin);
    }

    public function userListing(UserListingRequest $request): JsonResponse
    {
        $validated = $request->validated();

        $query = User::baseFilterSortsFromRequest($validated);

        return $this->responseSuccessWithData($query->get()->toArray());
    }

    private function allows(string $permission, User $user, User $onUser): bool
    {
        return Gate::allows('user-edit', $user) && !$onUser->isAdmin();
    }

    public function userEdit(CreateUserRequest $request, string $uuid): JsonResponse
    {
        $userToEdit = User::whereUuid($uuid)->first();

        if (! $userToEdit) {
            return $this->responseError('User for update not found', Response::HTTP_NOT_FOUND);
        }

        if (! $this->allows('user-edit', $request->user(), $userToEdit ) ) {
            return $this->responseError('Operation not allowed for user', Response::HTTP_METHOD_NOT_ALLOWED);
        }

        (new UserEdit())($userToEdit, $request->validated());

        $userUpdated = User::whereUuid($uuid)->first();

        return $this->responseSuccessWithData($userUpdated->toArray());
    }

    public function userDelete(Request $request, string $uuid): JsonResponse
    {

        $userToDelete = User::whereUuid($uuid)->first();
        if (! $userToDelete) {
            return $this->responseError('User for delete not found', Response::HTTP_NOT_FOUND);
        }

        if (! $this->allows('user-delete', $request->user(), $userToDelete ) ) {
            return $this->responseError('Operation not allowed for user', Response::HTTP_METHOD_NOT_ALLOWED);
        }

        (new UserDelete())($userToDelete);

        return $this->responseSuccess();
    }
}
