<?php

namespace App\Http\Controllers;

use App\Http\Requests\BrandsListRequest;
use App\Models\Brand;
use App\Http\Requests\StoreBrandRequest;
use App\Http\Requests\UpdateBrandRequest;
use App\Support\CustomApiResponse;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class BrandController extends Controller
{
    use CustomApiResponse;

    /**
     * Display a listing of the resource.
     */
    public function index(BrandsListRequest $request): JsonResponse
    {
        $validated = $request->validated();
        $query = Brand::baseFilterSortsFromRequest($validated);
        return $this->responseSuccessWithData($query->get()->toArray());
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(StoreBrandRequest $request): JsonResponse
    {
        $brand = Brand::create($request->validated());
        return $this->responseSuccessWithData($brand->fresh()->toArray(), Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     */
    public function show(Brand $brand): JsonResponse
    {
        return $this->responseSuccessWithData($brand->toArray());
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateBrandRequest $request, Brand $brand): JsonResponse
    {
        $brand->update($request->validated());
        return $this->responseSuccessWithData($brand->toArray());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Brand $brand): JsonResponse
    {
        $brand->delete();
        return $this->responseSuccess();
    }
}
