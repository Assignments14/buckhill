<?php

namespace App\Http\Controllers;

use App\Models\OrderStatus;
use App\Http\Requests\StoreOrderStatusRequest;
use App\Http\Requests\UpdateOrderStatusRequest;
use App\Support\CustomApiResponse;
use App\Support\FilterSortRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;

class OrderStatusController extends Controller
{
    use CustomApiResponse, FilterSortRequests;

    protected array $allowedFilters = [];

    protected array $allowedSorts = ['title', 'created_at'];

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): JsonResponse
    {
        $filteredSorted = $this->indexWithFilterSort($request, OrderStatus::class);
        return $this->responseSuccessWithData($filteredSorted);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function create(StoreOrderStatusRequest $request): JsonResponse
    {
        $status = OrderStatus::create($request->validated());
        return $this->responseSuccessWithData($status->fresh()->toArray());
    }

    /**
     * Display the specified resource.
     */
    public function show(OrderStatus $orderStatus): JsonResponse
    {
        return $this->responseSuccessWithData($orderStatus->toArray());
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateOrderStatusRequest $request, OrderStatus $orderStatus): JsonResponse
    {
        $orderStatus->update($request->validated());
        return $this->responseSuccessWithData($orderStatus->toArray());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(OrderStatus $orderStatus): JsonResponse
    {
        $orderStatus->delete();
        return $this->responseSuccess();
    }
}
