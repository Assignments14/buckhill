<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    public const HOME = '/home';

    public function boot(): void
    {
        RateLimiter::for('api', static function (Request $request) {
            return Limit::perMinute(60)->by($request->user()?->id ?: $request->ip());
        });

        $this->routes(function () {
            Route::middleware('api')
                ->prefix(env('API_PREFIX', 'api/v1'))
                ->group(base_path('routes/api.php'))
                ->group(base_path('routes/api/admin.php'))
                ->group(base_path('routes/api/user.php'))
                ->group(base_path('routes/api/category.php'))
                ->group(base_path('routes/api/brand.php'))
                ->group(base_path('routes/api/order_status.php'))
                ->group(base_path('routes/api/payment.php'))
                ->group(base_path('routes/api/product.php'))
                ->group(base_path('routes/api/file.php'))
                ->group(base_path('routes/api/main.php'))
                ->group(base_path('routes/api/order.php'))
                ->group(base_path('routes/api/swagger.php'));
        });
    }
}
