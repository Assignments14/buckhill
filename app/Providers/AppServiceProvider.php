<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Validator::extend('keys_in', static function ($attribute, $value, $arr, $validator) {
            if (!is_array($value)) {
                return false;
            }

            foreach (array_keys($value) as $key) {
                if (!in_array($key, $arr, true)) {
                    return false;
                }
            }

            return true;
        });
    }
}
