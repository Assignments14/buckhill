<?php

namespace App\Enums;

enum PaymentType: string
{
    case BankTransfer = 'bank_transfer';
    case CreditCard = 'credit_card';
    case CashOnDelivery = 'cash_on_delivery';
}
