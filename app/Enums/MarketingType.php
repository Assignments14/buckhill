<?php

namespace App\Enums;

enum MarketingType: int
{
    case NoMarketing = 0;
    case Marketing = 1;
}
