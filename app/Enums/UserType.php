<?php

namespace App\Enums;

enum UserType: int
{
    case Default = 0;
    case Admin = 1;
}