<?php

namespace App\Support;

class ApiResponse
{
    private static function stencil(
        int $success = -1,
        array $data = [],
        string $error = '',
        array $errors = [],
        array $extra = [],
    ): array
    {
        return compact('success', 'data', 'error', 'errors', 'extra');
    }

    public static function success(): array
    {
        return self::stencil(1);
    }

    public static function successData(array $data): array
    {
        return self::stencil(1, $data);
    }

    public static function error(string $error, array $errors = []): array
    {
        return self::stencil(success: 0, error: $error, errors: $errors);
    }
}