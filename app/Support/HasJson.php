<?php

namespace App\Support;

trait HasJson
{
    /**
     * @throws \JsonException
     */
    public function castJson(string $value): false|string
    {
        return json_encode(json_decode($value, true, 512, JSON_THROW_ON_ERROR), JSON_THROW_ON_ERROR);
    }
}