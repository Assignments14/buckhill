<?php

namespace App\Support;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\QueryBuilderRequest;

trait FilterSortRequests
{
    protected function appendFilterSortRequests(Request $request): void
    {
        // todo: filter by json fields
        // todo: sort by json fields
        // todo: check indexes on filtered and sorted fields

        $appends = [];

        foreach ($this->allowedFilters as $field) {
            $appends['filter'][$field] = $request->get($field);
        }

        if ($request->has('sortBy')) {
            $sortKey = "sort";
            $sortValue = $request->get('sortBy');
            if ($request->has('desc') && $request->get('desc') === '1') {
                $sortValue = '-' . $sortValue;
            }
            $appends[$sortKey][] = $sortValue;
        }

        $request->merge($appends);
    }

    protected function indexWithFilterSort(Request $request, $modelClass): array
    {
        $this->appendFilterSortRequests($request);

        $filtered = QueryBuilder::for($modelClass);

        if ($this->allowedFilters) {
            $filtered->allowedFilters($this->allowedFilters);
        }

        if ($this->allowedSorts) {
            $filtered->allowedSorts($this->allowedSorts);
        }

        if ($request->has('limit')) {
            $limit = (int) $request->get('limit');
            $filtered->limit($limit);
        }

//        $filtered->ddRawSql();

        return $filtered->simplePaginate()->items();
    }
}