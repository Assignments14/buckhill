<?php

namespace App\Support;

use Illuminate\Http\JsonResponse;

trait CustomApiResponse
{
    public function responseSuccess(): JsonResponse
    {
        return response()->json(ApiResponse::success());
    }

    public function responseSuccessWithData(array $data, int $status = 200): JsonResponse
    {
        return response()->json(
            ApiResponse::successData($data), $status
        );
    }

    public function responseError(string $error, int $status): JsonResponse
    {
        return response()->json(
            ApiResponse::error($error),
            $status
        );
    }
}